package com.weatherapp.Main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;
import org.json.JSONArray;
import org.json.JSONObject;

public class Weather {

    public static final String GET_METHOD = "GET";
    public static final String SEARCH = "search";
    public static final String CITY_ARGS = "city";
    public static final String COORDINATES_ARGS = "coordinates";
    public static final String SEARCH_API_CITY_URL = "https://www.metaweather.com/api/location/search/?query=";
    public static final String SEARCH_API_COORD_URL = "https://www.metaweather.com/api/location/search/?lattlong=";
    public static final String GET_CURRENT_CITY_BY_ID = "https://www.metaweather.com/api/location/";

    public static void main(String args[]) {
        try {
            String searchArgs = args[0];

            if (searchArgs.equals("search")) {
                switch (args[1]) {
                    case "city":
                        System.out.println("searching city");
                        String cityName = args[2];
                        searchByCity(cityName);
                        break;
                    case "coordinates":
                        System.out.println("searching coord");
                        String coord = args[2];
                        searchByCoordinates(coord);
                        break;
                    default:
                        break;
                }
            } else {
                System.out.println("AYAYA");
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    private static void searchByCity(String cityName) throws IOException, MalformedURLException {
        URL url = new URL(SEARCH_API_CITY_URL + cityName);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod(GET_METHOD);
        connection.setRequestProperty("Content-Type", "application/json; utf-8");
        connection.setRequestProperty("Accept", "application/json");
        connection.setDoOutput(true);
        int responseCode = connection.getResponseCode();

        BufferedReader reader;
        String line;
        StringBuffer responseContent = new StringBuffer();

        if (responseCode > 299) {
            reader = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
            while ((line = reader.readLine()) != null) {
                responseContent.append(line);
            }
            reader.close();
        } else {
            reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            while ((line = reader.readLine()) != null) {
                responseContent.append(line);
            }
            reader.close();
        }
        JSONArray arr = new JSONArray(responseContent.toString());
        JSONObject json = new JSONObject();
        Scanner userInput = new Scanner(System.in);
        for (int i = 0; i < arr.length(); i++) {
            json = arr.getJSONObject(i);
            String title = json.getString("title");

            System.out.println(i + 1 + ". " + title);

        }
        System.out.print("Which one of that you want to see (in number, ex: 1)? ");

        String input = userInput.nextLine();
        JSONObject city = arr.getJSONObject(Integer.parseInt(input) - 1);
//        System.out.println(city);
        searchCurrentCityById(city.getInt("woeid"));
    }

    private static void searchCurrentCityById(int woeid) throws MalformedURLException, IOException {
        String API_URL = GET_CURRENT_CITY_BY_ID + woeid;

        URL url = new URL(API_URL);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod(GET_METHOD);
        connection.setRequestProperty("Content-Type", "application/json; utf-8");
        connection.setRequestProperty("Accept", "application/json");
        connection.setDoOutput(true);
        int responseCode = connection.getResponseCode();

        BufferedReader reader;
        String line;
        StringBuffer responseContent = new StringBuffer();

        if (responseCode > 299) {
            reader = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
            while ((line = reader.readLine()) != null) {
                responseContent.append(line);
            }
            reader.close();
        } else {
            reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            while ((line = reader.readLine()) != null) {
                responseContent.append(line);
            }
            reader.close();
        }

//        JSONArray arr = new JSONArray(new JSONObject(responseContent));
        JSONObject json = new JSONObject(responseContent.toString());
        JSONArray currentTime = json.getJSONArray("consolidated_weather");
        JSONObject currentTimeObj = new JSONObject();
        System.out.print("Date : ");
        for (int i = 0; i < currentTime.length(); i++) {
            currentTimeObj = currentTime.getJSONObject(i);
            System.out.print(" " + currentTimeObj.getString("applicable_date") + " ");
        }
        System.out.print("\n");
        System.out.print("Weather : ");
        for (int i = 0; i < currentTime.length(); i++) {
            currentTimeObj = currentTime.getJSONObject(i);
            System.out.print(" " + currentTimeObj.getString("weather_state_name") + " ");
        }
        System.out.print("\n");
        System.out.print("Min. temp : ");
        for (int i = 0; i < currentTime.length(); i++) {
            currentTimeObj = currentTime.getJSONObject(i);
            System.out.print(" " + Math.round(currentTimeObj.getFloat("min_temp")) + "° ");
        }
        System.out.print("\n");
        System.out.print("Max. temp : ");
        for (int i = 0; i < currentTime.length(); i++) {
            currentTimeObj = currentTime.getJSONObject(i);
            System.out.print(" " + Math.round(currentTimeObj.getFloat("max_temp")) + "° ");
        }
        System.out.print("\n");
        System.out.print("Temp : ");
        for (int i = 0; i < currentTime.length(); i++) {
            currentTimeObj = currentTime.getJSONObject(i);
            System.out.print(" " + Math.round(currentTimeObj.getFloat("the_temp")) + "° ");
        }
    }

    private static void searchByCoordinates(String coord) throws MalformedURLException, IOException {
        URL url = new URL(SEARCH_API_COORD_URL + coord);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod(GET_METHOD);
        connection.setRequestProperty("Content-Type", "application/json; utf-8");
        connection.setRequestProperty("Accept", "application/json");
        connection.setDoOutput(true);
        int responseCode = connection.getResponseCode();

        BufferedReader reader;
        String line;
        StringBuffer responseContent = new StringBuffer();

        if (responseCode > 299) {
            reader = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
            while ((line = reader.readLine()) != null) {
                responseContent.append(line);
            }
            reader.close();
        } else {
            reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            while ((line = reader.readLine()) != null) {
                responseContent.append(line);
            }
            reader.close();
        }
        JSONArray arr = new JSONArray(responseContent.toString());
        JSONObject json = new JSONObject();
        Scanner userInput = new Scanner(System.in);
        for (int i = 0; i < arr.length(); i++) {
            json = arr.getJSONObject(i);
            String title = json.getString("title");

            System.out.println(i + 1 + ". " + title);

        }
        System.out.print("Which one of that you want to see (in number, ex: 1)? ");

        String input = userInput.nextLine();
        JSONObject city = arr.getJSONObject(Integer.parseInt(input) - 1);
        searchCurrentCityById(city.getInt("woeid"));
    }
}
